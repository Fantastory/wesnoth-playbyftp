<?php

namespace Fns;

class Strict {
	public function __get(string $name) {
		\Logger::Fatal('try to get non existing field '.$name);
		throw new \Exception('try to get non existing field '.$name);
	}

	public function __set(string $name, $value) {
		\Logger::Fatal('try to set non existing field '.$name);
		throw new \Exception('try to set non existing field '.$name);
	}
}

class Decl {
	public $fnFields = [];

	public function __construct(array $fields) {
		$this->fnFields = $fields;
	}

	public function __get(string $name) {
		if (isset($this->fnFields[$name])) return $this->fnFields[$name];

		\Logger::Fatal('try to get non existing field '.$name);
		throw new \Exception('try to get non existing field '.$name);
	}

	public function __set(string $name, $value) {
		if (isset($this->fnFields[$name])) {
			$this->fnFields[$name] = $value;
			return $this->fnFields[$name];
		}

		\Logger::Fatal('try to set non existing field '.$name);
		throw new \Exception('try to set non existing field '.$name);
	}
}
