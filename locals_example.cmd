set PLAYER_NAME=%username%
set PLAYER_NAME=player1
set NEXT_PLAYER=player2

set SAVE_PREFIX=User Map-A
set GAME_USER_DATA_DIR=~d0/mgame-data

set WESNOTH_EXE=C:\Program Files (x86)\Battle for Wesnoth\wesnoth.exe

set FTP_ADDRESS=ftp.yourserver.localhost
set FTP_LOGIN=your_ftp_login
set FTP_PASSWORD=secret_password
set FTP_DIRECTORY=/public_html/games/wesnoth/saves
set HTTP_DIRECTORY=http://yourserver.com/games/wesnoth/saves1/

set WHO_PLAYS_FILE=whoplays.html
