<?php
require_once("bootstrap.php");
require_once("SaveInfo.php");

\Logger::$Instance->setLogLevel('debug');

\Logger::Info("started");
\Logger::Info("ftp_addres ".\Enviroment::$Env->ftp_address);

$command = '"'.\Enviroment::$Env->wesnoth_exe
	.'" -w --userdata-dir="'.\Enviroment::$Env->game_user_data_dir;
\Logger::Info('execute '.$command);
$output = shell_exec($command);
\Logger::Info($output);

$local_save = SaveInfo::GetLocal();
if ($local_save->turn < 0) throw new \Exception('no local save after sync');

if (true) {
	\Logger::Info('ftp put '.$local_save->name);
	$ftp = SaveInfo::GetFtp();
	ftp_put($ftp, basename($local_save->name), $local_save->name); 
	$remote_save = SaveInfo::GetLocal();

	SaveInfo::MarkTurn($ftp, $local_save);
}

\Logger::Info("done");
