<?php

require_once('FnsLogger.php');
require_once('Strict.php');

ini_set('track_errors', 1);

class Enviroment extends \fns\Strict {
	static $Env = null;

	public $player_name = '';
	public $next_player = '';
	public $save_prefix = '';

	public $game_user_data_dir = '';
	public $wesnoth_exe = '';

	public $ftp_address = '';
	public $ftp_login = '';
	public $ftp_password = '';
	public $ftp_directory = '';
	public $http_directory = '';
	public $who_plays_file = '';

	static function loadEnv(string $name) : string {
		$value = getenv($name);
		if ($value == FALSE) throw new Exception("getenv($name)");
		return $value;
	}

	function __construct() {
		$this->player_name    = Enviroment::loadEnv('PLAYER_NAME');
		$this->next_player    = Enviroment::loadEnv('NEXT_PLAYER');
		$this->save_prefix    = Enviroment::loadEnv('SAVE_PREFIX');
		$this->game_user_data_dir = Enviroment::loadEnv('GAME_USER_DATA_DIR');
		$this->wesnoth_exe    = Enviroment::loadEnv('WESNOTH_EXE');

		$this->ftp_address    = Enviroment::loadEnv('FTP_ADDRESS');
		$this->ftp_login      = Enviroment::loadEnv('FTP_LOGIN');
		$this->ftp_password   = Enviroment::loadEnv('FTP_PASSWORD');
		$this->ftp_directory  = Enviroment::loadEnv('FTP_DIRECTORY');
		$this->http_directory = Enviroment::loadEnv('HTTP_DIRECTORY');
		$this->who_plays_file = Enviroment::loadEnv('WHO_PLAYS_FILE');
	}
}

if (\Enviroment::$Env == null) {
	\Enviroment::$Env = new Enviroment();
}

function startsWith(string $string, string $startString) : bool
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
}

function endsWith($string, $endString) 
{ 
    $len = strlen($endString); 
    if ($len == 0) { 
        return true; 
    } 
    return (substr($string, -$len) === $endString); 
} 