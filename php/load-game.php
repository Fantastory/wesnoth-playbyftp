<?php
require_once("bootstrap.php");
require_once("SaveInfo.php");

\Logger::$Instance->setLogLevel('debug');

\Logger::Info("started");
\Logger::Info("ftp_addres ".\Enviroment::$Env->ftp_address);

$local_save = SaveInfo::GetLocal();

$ftp = SaveInfo::GetFtp();
$remote_save = SaveInfo::GetRemote($ftp);
if ($remote_save->greaterThan($local_save)) {
	\Logger::Info("ftp file is newer download");
	$bret = ftp_get($ftp, SaveInfo::LocalSaveDirectory().$remote_save->name, $remote_save->name); 
	if ($bret === false) throw new \Exception("ftp_ssl_connect(".\Enviroment::$Env->ftp_directory.")");
	$local_save = SaveInfo::GetLocal();
}
ftp_close($ftp);

if ($local_save->turn < 0) throw new \Exception('no local save after sync');

$command = '"'.\Enviroment::$Env->wesnoth_exe
	.'" -w --userdata-dir="'.\Enviroment::$Env->game_user_data_dir
	.'" --load="'.basename($local_save->name).'"';

\Logger::Info('execute '.$command);
$output = shell_exec($command);
\Logger::Info($output);

$local_save = SaveInfo::GetLocal();
if ($local_save->turn < 0) throw new \Exception('no local save after sync');

if ($local_save->greaterThan($remote_save)) {
	\Logger::Info('ftp put '.$local_save->name);
	$ftp = SaveInfo::GetFtp();
	ftp_put($ftp, basename($local_save->name), $local_save->name); 
	$remote_save = SaveInfo::GetLocal();

	SaveInfo::MarkTurn($ftp, $local_save);
}

\Logger::Info("done");
