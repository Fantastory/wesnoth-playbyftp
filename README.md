# wesnoth-playbyftp

1. Get an ftp server
2. rename locals_example.cmd to locals.cmd
3. fill parameters in locals.cmd
4. launch prepare-game.bat and setup map and opponents
5. recheck autosave file name against locals.cmd
4. load-game.bat will
	a) sync save from ftp
	b) run the game
	c) in game when you end the turn it will be stored in autosave
	d) exit game
	e) savefile will be uploaded to ftp
5. check whoplays.bat to know whos turn it is

