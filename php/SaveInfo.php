<?php

class SaveInfo extends \fns\Strict {
	public $turn = -1;
	public $name = 'empty';
	public $modify = -1;

	public static $SaveRegex = '/.*[^\d](?<turn>\d+)\.gz$/';

	function greaterThan(SaveInfo $other) : bool {
		//if ($this->turn > $other->turn) return true;
		//if ($this->turn < $other->turn) return false;
		if ($this->modify > $other->modify) return true;

		return false;
	}

	static function GetFtp() {
		//$ftp = ftp_ssl_connect(\Enviroment::$Env->ftp_address);
		$ftp = ftp_connect(\Enviroment::$Env->ftp_address);
		if ($ftp === false) throw new \Exception("ftp_ssl_connect(".\Enviroment::$Env->ftp_address.")");
		$bret = ftp_login($ftp, \Enviroment::$Env->ftp_login, \Enviroment::$Env->ftp_password);
		if ($bret === false) throw new \Exception("ftp_ssl_connect(".\Enviroment::$Env->ftp_login.")");
		//the direcotry must be prepared before
		//we do not create directories ourself in case there is a misspell in configuration 
		$bret = ftp_chdir($ftp, \Enviroment::$Env->ftp_directory);
		if ($bret === false) throw new \Exception("ftp_ssl_connect(".\Enviroment::$Env->ftp_directory.")");

		return $ftp;
	}

	static function LocalSaveDirectory() : string {
		return \Enviroment::$Env->game_user_data_dir.'/saves/';
	}

	static function GetLocal() : SaveInfo {
		$local_saves_pattern = self::LocalSaveDirectory().\Enviroment::$Env->save_prefix.'*.gz';
		\Logger::Debug("local_saves_pattern $local_saves_pattern");
		\Logger::Debug("save regex ".self::$SaveRegex);

		$latest = new SaveInfo();
		foreach (glob($local_saves_pattern) as $filename) {
			\Logger::Debug("file ".$filename);
			$matches = array();
			$pregret = preg_match(SaveInfo::$SaveRegex, $filename, $matches);
			if ($pregret === false) throw new \Exception('preg_match('.Self::$SaveRegex.')'); 
			if ($pregret == 0) continue;
	
			$compare = new SaveInfo();
			$compare->turn = intval($matches['turn']);
			if ($compare->turn < 0) {
				throw new \Exception(
					'turn for save '.$filename.' <0 == '.$compare->turn
				);
			}
			$compare->name = $filename;
			$compare->modify = filemtime($compare->name);
			if ($compare->modify === false) throw new \Exception('filemtime('.$compare->name.')'); 

			if ($compare->greaterThan($latest)) {
				$latest = $compare;
			}
		}

		if ($latest->turn < 0) \Logger::Info('no local save games');
		else \Logger::Info('local save is '.$latest->name);
		return $latest;
	}

	static function GetRemote($ftp) : SaveInfo {
		\Logger::debug('call ftp_mlsd');
		$remote_files = @ftp_mlsd($ftp, '.');
		if ($remote_files === false) {
			throw new \Exception("ftp_mlsd ".print_r(error_get_last(),true).' '.print_r($remote_files,true));
		}

		$latest = new SaveInfo();
		foreach($remote_files as $remote_file) {
			\Logger::Debug("file ".$remote_file['name']);
			if (!startsWith($remote_file['name'], \Enviroment::$Env->save_prefix)) continue;
			$matches = array();
			$pregret = preg_match(self::$SaveRegex, $remote_file['name'], $matches);
			if ($pregret === false) throw new \Exception("preg_match(".self::$SaveRegex.")"); 
			if ($pregret == 0) continue;
		
			$compare = new SaveInfo();
			$compare->turn = intval($matches['turn']);
			if ($compare->turn < 0) {
				throw new \Exception(
					'turn for save '.$remote_file['name'].' <0 == '.$compare->turn
				);
			}
			$compare->name = $remote_file['name'];
			$timestr = $remote_file['modify'];
			//20171212154511
			//Y   m d H i s
			$time = DateTime::createFromFormat('YmdHis', $timestr);
			$compare->modify = $time->getTimestamp();

			$older = $compare;
			if ($compare->greaterThan($latest)) {
				$older = $latest;
				$latest = $compare;
			}
			if ($older->turn >= 0) {
				$bret = ftp_delete($ftp, $older->name);
				if (!$bret) {
					throw new \Exception
					//\Logger::Fatal
					("ftp_delete ".$older->name);	
				}
			}

		}

		if ($latest->turn < 0)	\Logger::Info('no remote save file');
		else \Logger::Info('remote save is '.$latest->name);
		return $latest;
	}

	static function MarkTurn($ftp, SaveInfo $saveInfo) {
		$prev_player = \Enviroment::$Env->player_name;		
		$next_player = \Enviroment::$Env->next_player;
		$current_time = new DateTime();
		$current_time = $current_time->format('Y-m-d H:i:s'); 
		$save_time = new DateTime();
		$save_time->setTimestamp($saveInfo->modify);
		$save_time = $save_time->format('Y-m-d H:i:s'); 

		$local_file = \SaveInfo::LocalSaveDirectory().\Enviroment::$Env->who_plays_file;
		$file = fopen($local_file, "w");
		if ($file === false) throw new \Exception("");
		fwrite($file, 
			<<<"EOD"
			<html>
				<head>
					<meta http-equiv='Cache-Control' content='no-cache, no-store, must-revalidate' />
					<meta http-equiv='Pragma' content='no-cache' />
					<meta http-equiv="Expires" content="0" />
					<meta http-equiv="refresh" content="120" />
					<title>$next_player's turn</title>
				</head>
				<body>
					<br/>$current_time
					<br/>$prev_player moved
					<br/>{$saveInfo->name} at $save_time
					<br/>
					<br/>it is turn for $next_player
				</body>
			</html>
EOD
		);

		ftp_put($ftp, \Enviroment::$Env->who_plays_file, $local_file); 
	}

}

