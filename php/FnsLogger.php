<?php

class FnsLogLevel {
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
    const TRACE     = 'trace';
    const FINER     = 'finer';
    const FINEST    = 'finest';
}

class FnsLogger //implements \Psr\Log\LoggerInterface
{
    public $log_level_int = 6;

    public static function GetTrace() {
        $backTrace = debug_backtrace();
        $level = 0;

        $callerTrace = null;
        while (true) {
            if ($level >= count($backTrace)) break;
            $callerTrace = $backTrace[$level];
            if (strpos(strtolower($callerTrace["class"]), 'logger')) {
                $level++;
                continue;
            }
            break;
        }
        return $callerTrace; 
    }

    public static function LevelToSyslog($level) {
        switch ($level) {
            case FnsLogLevel::EMERGENCY: return 0;
            case FnsLogLevel::ALERT    : return 1;
            case FnsLogLevel::CRITICAL : return 2;
            case FnsLogLevel::ERROR    : return 3;
            case FnsLogLevel::WARNING  : return 4;
            case FnsLogLevel::NOTICE   : return 5;
            case FnsLogLevel::INFO     : return 6;
            case FnsLogLevel::DEBUG    : return 7;
            case FnsLogLevel::TRACE    : return 8;
            case FnsLogLevel::FINER    : return 9;
            case FnsLogLevel::FINEST    : return 10;
            default                    : return 11;
        }
    }

    public static function LevelToE_USER_($level) {
        switch ($level) {
            case FnsLogLevel::EMERGENCY: return E_USER_ERROR;
            case FnsLogLevel::ALERT    : return E_USER_ERROR;
            case FnsLogLevel::CRITICAL : return E_USER_ERROR;
            case FnsLogLevel::ERROR    : return E_USER_ERROR;
            case FnsLogLevel::WARNING  : return E_USER_WARNING;
            case FnsLogLevel::NOTICE   : return E_USER_NOTICE;
            case FnsLogLevel::INFO     : return E_USER_DEPRECATED;
            case FnsLogLevel::DEBUG    : return E_USER_DEPRECATED;
            case FnsLogLevel::TRACE    : return E_USER_DEPRECATED;
            default                    : return E_USER_DEPRECATED;
        }
    }

    protected function plog($level, $message, array $context = array())
    {
        //$e_user_level = self::LevelToE_USER_($level);
        $level_int = self::LevelToSyslog($level);
        if ($level_int > $this->log_level_int) return;

        $trace = self::GetTrace();

        $file = $trace['file'];
        //$referenceDir = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR;
        //if (strpos($file, $referenceDir) === 0) {
        //    $file = substr($file, strlen($referenceDir));
        //}
        $full_message = $level."\t".$file.':'.$trace['line']."\t".$message;
        if ($context && count($context) > 0){
            $full_message .= "\t".print_r($context, true);
        }

        error_log($full_message);
    }

    public function setLogLevel($level) {
        if (is_int($level)) {
            $this->log_level_int = $level;
        } else {
            $this->log_level_int = self::LevelToSyslog($level);
        }
    }

    public function log($level, $message, array $context = array())
    {
        $this->plog($level, $message, $context);
    }

    public function emergency($message, array $context = array())
    {
        $this->plog(FnsLogLevel::EMERGENCY, $message, $context);
    }

    public function alert($message, array $context = array())
    {
        $this->plog(FnsLogLevel::ALERT, $message, $context);
    }

    public function critical($message, array $context = array())
    {
        $this->plog(FnsLogLevel::CRITICAL, $message, $context);
    }

    public function error($message, array $context = array())
    {
        $this->plog(FnsLogLevel::ERROR, $message, $context);
    }

    public function warning($message, array $context = array())
    {
        $this->plog(FnsLogLevel::WARNING, $message, $context);
    }

    public function notice($message, array $context = array())
    {
        $this->plog(FnsLogLevel::NOTICE, $message, $context);
    }

    public function info($message, array $context = array())
    {
        $this->plog(FnsLogLevel::INFO, $message, $context);
    }

    public function debug($message, array $context = array())
    {
        $this->plog(FnsLogLevel::DEBUG, $message, $context);
    }

    public function trace($message, array $context = array())
    {
        $this->plog(FnsLogLevel::TRACE, $message, $context);
    }
}

//we need global loger
//so everywhere in a code we can write
//  \Logger::info('the info')
class Logger {
    public static $Instance;

    public static function setLogger($logger) {
        self::$Instance = $logger;
    }

    public static function __callStatic($name, $arguments) {
        $name = strtolower($name);
        if ($name == 'fatal') $name = 'critical';
        return call_user_func_array([self::$Instance, $name], $arguments);
    }
}

if (!Logger::$Instance) Logger::$Instance = new FnsLogger();


